# Lab: Explore Wireshark with DNS
## Introduction
In this lab, I examined different types of DNS queries and responses including Type=A and Type=NS.
## Collaboration
I did not use any additional resources for this lab.
## Process
I followed the instructions from the [course website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php).
## Questions
### 1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in
The IP address of www.iitb.ac.in is 103.21.124.10.
### 2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?
The IP address of the DNS server is 10.1.74.10.
### 3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?
It did not come from an authoritative server because it says "Non-authoritative answer" in front of the IP address. This is also a Type=A request which does not return an authoritative server.
### 4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?
dns1.iitb.ac.in
To find the IP address, we could use nslookup with this hostname.
### 5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?
It is packet number 15 and this message was sent over UDP.
### 6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?
It is packet number 17 and the message was sent over UDP
### 7. What is the destination port for the DNS query message? What is the source port of the DNS response message?
Both are port 53.
### 8. To what IP address is the DNS query message sent?
75.75.75.75
### 9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
There is one question and 0 answers.
### 10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
There is one question and one answer.
### 11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.
Packet number 22 is the initial HTTP GET request for the base file. Packet number 15 is the DNS query made to resolve gaia.cs.umass.edu. Packet number 17 is the received DNS response. Packet number 205 is the HTTP GET request for the image object. There is no packet for the DNS query made to resolve gaia.cs.umass.edu the second time because it is already stored in the host's DNS resolver cache. Since it was already in the cache, there was no need to contact the DNS server a second time.
### 12. What is the destination port for the DNS query message? What is the source port of the DNS response message?
Both are port 53, the commonly used port for DNS.
### 13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
75.75.75.75 - Since I am using the pre-captured file, this is not the IP address of my local DNS server. However, I used nslookup to discover the hostname of this IP address and it looks like it could be someone's local DNS server.
### 14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
The Type=A, meaning it is requesting an IP address that maps to the givne hostname. There are no answers in the query.
### 15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
There is one question and one answer. 
### 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
75.75.75.75 - I am using the pre-captured file and the IP address is the same as the previous file. I am assuming that this IP address is still the default local DNS server.
### 17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?
There is one question and no answers.
### 18. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?
There are 3 answers and 3 additional resource records. The answers include 3 authoritative servers of the hostname from the DNS query. The additional resource records include the IP addresses of these 3 authoritative servers.