# Week 3: The Application Layer - DNS and Other Protocols

This week we learned about other Application Layer protocols such as SMTP, IMAP, and DNS.

- [Learning Objectives](learning-objectives.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Exploring DNS with Wireshark](lab-wireshark-dns)