# Week 4: The Transport Layer - TCP and UDP

This week we learned more about TCP/UDP differences such as multiplexing and examples of reliable data transfer.

- [Learning Objectives](learning-objectives.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Exploring TCP with Wireshark](lab-wireshark-tcp)