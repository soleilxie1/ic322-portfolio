# Introduction
In this lab, I examined TCP packets through HTTP by uploading a text document using the POST request.
# Collaboration
No other external sources were used.
# Process
I followed the instructions from the [textbook website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php).
# Questions
## 1 What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?  To answer this question, it’s probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the “details of the selected packet header window” (refer to Figure 2 in the “Getting Started with Wireshark” Lab if you’re uncertain about the Wireshark windows).
The source IP address is 10.25.148.106 and the source port is 51870. 
## 2 What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
The IP address of gaia.cs.umass.edu is 128.119.245.12 and the port number it is using for TCP segments is 80.
## 3 What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? (Note: this is the “raw” sequence number carried in the TCP segment itself; it is NOT the packet # in the “No.” column in the Wireshark window.  Remember there is no such thing as a “packet number” in TCP or UDP; as you know, there are sequence numbers in TCP and that’s what we’re after here.  Also note that this is not the relative sequence number with respect to the starting sequence number of this TCP session.). What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.5 in the text)?
The sequence number of the TCP segment is 0. This is because this is the initial segment used to initiate the TCP connection. The segment has length 0, meaning this is a SYN segment because no payload is included. This can be identified as a SYN segment because there is a SYN flag in the flags section.
## 4 What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value? 
The sequence number is 0 and it includes SYN and ACK flags in the flag section of the segment. The value of the Acknowledgement field is 1. This is because it received the previous SYN segment.
## 5 What is the sequence number of the TCP segment containing the header of the HTTP POST command?  Note that in order to find the POST message header, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with the ASCII text “POST” within its DATA field,.  How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?
The sequence number of the the HTTP POST segment is 1. The number of bytes included in this segment is 707 bytes. Not all of the data from alice.txt was able to fit in this single segment since the file size of alice.txt is around 150 KB.
## 6 Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.  
### a At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent?
The time was 10:57:54.759.  
### b At what time was the ACK for this first data-containing segment received?  
It was received 10:57:54.762.
### c What is the RTT for this first data-containing segment? 
The RTT for this segment was .018484 seconds.
### d What is the RTT value the second data-carrying TCP segment and its ACK? 
The RTT for this segment was .018484 seconds, same as the previous segment.
### e What is the EstimatedRTT value (see Section 3.5.3, in the text) after the ACK for the second data-carrying segment is received? Assume that in making this calculation after the received of the ACK for the second segment, that the initial value of EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation on page 242, and a value of α = 0.125.
(1-a) * ERTT + a * SampleRTT = (1-.125) * .018484 + .125 + .018484
= .018484 seconds

Since the last two segments had the same RTT, the EstimatedRTT is the same as the last two segments.
## 7 What is the length (header plus payload) of each of the first four data-carrying TCP segments?
Segment 1: 727 bytes
Segment 2: 1400 bytes
Segment 3: 1400 bytes
Segment 4: 1400 bytes

All headers were 20 bytes.
## 8 What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?
The minimum amount of available buffer space is 512 bytes, shown in the image below:

![buffer](<lab-buffer.jpg>)

There doesn't seem to be any throttling of the sender since it never reaches the maximum buffer space.
## 9 Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
Yes, I looked for Duplicate ACKs and there was one for the segment 1. This may mean that the first segment was lost.
## 10 How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?
It looks like it the receiver does not acknowledge the segments until after the tenth data-carrying segment. However, after this ACK, it starts to ACK every other segment for a little bit.
## 11 What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.
1380 bytes / .018484 seconds = 74,659 bytes/second

I used the TCP payload in one of the segments and the RTT value to calculate this value.
## 12 Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.
![figure6](<lab-fig6.jpg>)

From t = 0.025 to .053, it seems like TCP is in its slow start phase because there is more time between packets being sent. There seems to be the start of the congestion avoidance phase at t = 0.1 because strings of fewer packets are sent more frequently to avoid congestion.
## 13 These “fleets” of segments appear to have some periodicity. What can you say about the period?
The period seems to be around .02 to .03 seconds between the fleets. 