# Week 10 Chapter Questions
## R11 How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
BGP uses the NEXT-HOP attribute to determine which router is the first on its path to a specific prefix. It is the IP address of the interface of the router that begins the AS-PATH. BGP uses the AS-PATH attribute to determine which AS's a packet would need to go through to reach its destination prefix.
## R13 True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
False, if the BGP router receives the path via eBGP (meaning it came from a different AS), the router sends the path it received to all routers in its same AS. All the routers in that AS now know how to get to the advertised path. If a gateway router receives the path via iBGP and plans to advertise the given router to another AS, it will add its own AS's identity and will send the path to its connected AS's. 
## R19 Names four different types of ICMP messages (also describe what they are used for)
- **Type 0, Code 0**: echo reply, used to reply to ping requests (Type 8).
- **Type 8, Code 0**: ping (echo request), used to check if a connection is established.
- **Type 3, Code 0**: destination network unreachable, an error message that tells the host that the destination network cannot be reached.
- **Type 3, Code 1**: destination host unreachable, an error message that tells the host that the destination host cannot be reached.
## R20 What two types of ICMP messages are received at the sending host executing the Traceroute program?
Type 11, Code 0 (TTL expired) is sent to source to obtain the roundtrip time to the nth router. When a traceoute is initiated, the source sends a series of UDP datagrams with increasing TTL values, starting at 1. The TTL will increase with each datagram and will reach the n+1 router each time.

Type 3, Code 3 (destination port unreachable) is used to let the source host know when to stop sending UDP segments. As the host continues to send increasing TTL fields, a datagram will eventually reach the desired destination. The destination sends this destination port unreachable message back to the source to let the source host know that it does not need to send more UDP datagrams. In reality, the purpose of the message does not match its description because it is very unlikely that the destination port will be unreachable since an uncommon one will be chosen. 
## P14 Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.
### a. Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?
eBGP, since x is in a different autonomous system and 3c is a gateway router connected to AS4 which is where it learns about x.
### b. Router 3a learns about x from which routing protocol?
iBGP, since 3a is in AS3 and learns about x through 3c using intra-AS routing.
### c. Router 1c learns about x from which routing protocol?
eBGP, since it learns about x from AS3 and 1c is the gateway router connected to 3a.
### d. Router 1d learns about x from which routing protocol?
iBGP, since 1d is in AS1 and it learns about x from 1c using intra-AS routing.  
## P15 Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.
### a. Will I be equal to I1 or I2  for this entry? Explain why in one sentence.
I1 because that is the closest interface to the gatway router 1c.
### b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I1 or I2? Explain why in one sentence.
I2 because there are less links connecting 1d to x from that interface compared to I1.
## c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence.
I1 because it takes the least number of AS's to get to 1d.
## P19 In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
A should advertise to B that it is connected to both W and V. To C, A should advertise that it is only connected to V. C receives an AS route to V from A, but not to W.
## P20 Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
No BGP does not allow Z to implement this policy since Z is not directly connected to X. Since Z is connected to X through Y, the only way for traffic coming to Z to get to X is through Z and then to Y to X. There is no other path for X's traffic to go.