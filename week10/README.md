# Week 10: The Network Layer - More Control Plane

This week, we learned about Autonomous Systems and their significance in the BGP protocol. We also learned about the ICMP protocol and what it is commonly used for.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Beta Tester](lab-beta-tester)
- [Community Garden](https://gitlab.usna.edu/ckoutrakos/ic322-portfolio/-/issues/22)