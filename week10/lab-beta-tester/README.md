# Lab: Beta Tester
## Introduction
This week, I remastered my Protocol Pioneer chapter 5 since my old solution was not very robust.
## Citation
I referenced LCDR Downs' Chapter 5 solution posted on the [protocol pioneer gitlab](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads).
## Process
```
import json
import random
# Welcome to the StellarScript Console!
#
# In this chapter you have 3 strategies to program: yourself, the drones, and Sawblade.
# 
# Nodes have random IDs in this level. They change every time you restart the level, but they're constant during
# a single play-through.
def createMessage(src, dest, type, payload):
    message = {
        "Source" : src,
        "Dest" : dest,
        "Type" : type,
        "Payload" : payload}

    return json.dumps(message)

def parseMessage(message):
    parsed = json.loads(message)
    return {"Source" : parsed["Source"], 
            "Dest" : parsed["Dest"],
            "Type" : parsed["Type"],
            "Payload" : parsed["Payload"] }
def dvMessage(src, dest, dv):
    return createMessage(src, dset, "DV", dv)

def updateDv(oldDv, newDv, interface):
    for id in newDv.keys():
        if id not in oldDv:
            oldDv[id] = [newDv[id][0] + 1, interface]
        else:
            if newDv[id][0] + 1 < oldDv[id][0]:
                oldDv[id][0] = newDv[id][0] + 1
                oldDv[id][1] = interface

def keyMessage(src, dest, key, exp):

    keyStr = {"Key": key, "Expiration": exp}
    return createMessage(src, dest, "Data", keyStr)
def drone_strategy(self):
    """Drones are responsible for routing messages."""
    counter = 0
    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    number_of_connected_interfaces = len(self.connected_interfaces())
    print(f"Drone {self.id} has {number_of_connected_interfaces} connected interfaces: {self.connected_interfaces()}")

    if "DV" not in self.state:
        self.state["DV"] = {self.id: [0, "X"]}
    sent = False
    while (self.message_queue):
        m = self.message_queue.pop()
        parse = parseMessage(m.text)

        if parse["Type"] == "DV":
            updateDv(self.state["DV"], parse["Payload"], m.interface)
        elif parse["Type"] == "Data":
            if parse["Dest"] in self.state["DV"].keys():
                self.sendMessage(parse["Dest"], self.state["DV"][parse["Dest"]][1])
                sent = True
    if not sent:
        m = DvMessage("0", "0", self.state["DV"])
        interface = random.choice(self.connected_interfaces())
        self.send_message(m, interface)



def player_strategy(self):
    """This is you. You have access to the key-generating machine. You need to send that key to Sawblade."""
    # `self.generate_key()` generates a new key. The function returns a tuple: (key, expiration). The first value
    # is the actual key (a number). The second value is the time that the key expires.
    # Every time you call the function, a new key is generated and the expiration date is set 18 ticks into the future.
    if "DV" not in self.state:
        self.state["DV"] = {self.id: [0, "X"]}
    sent = False
    # If you want, you can use your heartbeat to count ticks.
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
        time = self.state["ticks"]
        if self.state["ticks"] % 20 == 0:
            key_expiration = self.generate_key()
            key = key_expiration[0]
            expiration = key_expiration[1]
            print(f"Player: key {key} expires at {expiration}.")
            m = keyMessage(source="1", dest="2", key=key, expiration)
            self.send_message(m, "E")

    while(self.message_queue):
        m = self.message_queue.pop()
        parsed = parse_message(m.text)
        if parsed["Type"] == "DV":
            updateDv(self.state["DV"], parsed["Payload"], m.interface)
        elif parsed["Type"] = "Data:
            if parsed["Dest"] in self.state["DV"].keys():
                self.sendMessage(parsed["Dest"], self.state["DV"][parsed["Dest"]][1])
                sent = True

    if not sent:
        m = DvMessage("0", "0", self.state["DV"])
        interface = random.choice(self.connected_interfaces())
        self.send_message(m, interface)
    print(f"The current time is {self.state['ticks']}...")

    

def sawblade_strategy(self):
    """Sawblade operates the radio, but need an unexpired key."""
    if "DV" not in self.state:
        self.state["DV"] = {self.id: [0, "X"]}
        
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    
    # Sawblade has the `self.operate_radio(key)` method available to her. Call it with the value of the key that the
    # player has. Don't use an expired key, the escape pod will go on permanent lock down!
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Sawblade: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        parsed = parse_message(m.text)
        if parsed["Type"] == "DV":
            updateDv(self.state["DV"], parsed["Payload"], m.interface)
        elif parsed["Type"] = "Data:
            if parsed["Dest"] == self.id:
                key = parsed["Payload"]["Key"]
                exp = parsed["Payload"]["Expiration"]
                ticks = self.state["ticks"]

                ticks <= exp:
                    self.operate_radio(key)
        

s = Chapter5(drone_strategy, player_strategy, sawblade_strategy, animation_frames=10, wait_for_user=False)
s.run()


```
I referenced LCDR Downs' solution to make my solution. I included a DV "table" of entries for each drone and updated it each time a new packet was received. There were two types of data: "DV" and "data". DV was for DV updating packets, where drones could know their relative paths to surrounding drones. Data included the key and its expiration. If sawblade received the "Data" packet, she would check to make sure it was not expired. If it wasn't expired, she would send it over the radio. 
## Question
###  1. How did you solve Chapter 5? Please copy and paste your winning strategy, and also explain it in English.
###  2. Beta Testing Notes
I thought there was a big jump in difficulty from Chapter 4 to Chapter 5. I don't think I would have been able to come up with my current Chapter 5 solution without looking at the solution provided.