# Chapter Questions
## R1 What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?

There is no difference between a host and an end system. The terms "host" and "end system" are both used to identify devices that live on the network edge. These devices can include home PCs, smartphones, refrigerators, web servers, etc. End systems/hosts are usually divded into two categories: clients and server. A web server falls under the server category, making it a part of the end system family. 
## R4 List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

The four access technologies are DSL, Cable, Ethernet, and 5G Wireless. 

DSL, digital subscriber line, is used when a local telephone company that provides local phone access also provides Internet access to the same residence. The same telephone company also becomes this resident's ISP. Using a DSLAM (digital subscriber line access multiplexer), the DSL modem translates the data from the telephone lines and translates it to high-frequency tones. High-speed downstream data is sent at 50kHz to 1MHz. Medium-speed upstream data is sent at 4kHz to 50 kHz. This allows any telephone calls and Internet access to exist simultaneously, since they are sent at different signals. This is commonly used as home access, since many home residents already have an existing telephone line.

Cable Internet access uses a resident's cable television company. This access technology is similar to the DSL, except instead of getting internet access from telephone line, a resident would get it from the cable television company. Most commonly, fiber optics connect the cable head end to neighborhoods, and a coaxial cable is used to reach individual residents. Known as a hybrid fiber coax (HFC), this system uses both fiber and coaxial cable. Cable access uses cable modems which divide data into upstream and downstream channels. This is commonly used as home access, since many home residents already have an existing cable line.

Ethernet is an access technology that is commonly used on corporate and university campuses. A local area network (LAN) containing an ethernet switch is connected to the Institutional router, which is then connected to the Institution's ISP. Users have around 100 Mbps to 10 Gpbs to the ethernet switch, while serves have around 1 Gbps to 10 Gbps. While Ethernet is used mainly as enterprise access, it is increasingly more popular for home access.

5G Wireless is an access technology that is commonly used by mobile devices for messaging, apps, streaming, etc. This access technology utilizes the cellular telephony infrastructure owned and operated by the cellular network provider. 5G wireless has the highest download speed of all previous generations, greater than 60 Mbps. Because users only need to be within tens of kilometers of a base station, this access technology is categorized as wide-area wireless access. Meanwhile, WiFi users need to be within a few tens of meters of the base station.


## R11 Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet length L?
![transmission rate](<transmission-rate.png>)
## R12 What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?
In circuit-switched networks, communication resources such as buffers and link transmission rate are reserved during each communcation session between two hosts. As a result, packet-switched networks experience delays when resources become unavailable due to not being reserved. In circuit-switched networks, a transmission rate is reserved, guaranteeing a constant rate of data flow between two end systems.

In Frequency-division multiplexing (FDM), different frequencies are divided and assigned to different connections across a given link. Connections are given a certain bandwidth in which data can be transmitted. For example, FM radio uses the frequencies between 88MHz and 108 MHz, making a bandwidth of 20 MHz. In time-division multiplexing (TDM), time is allocated into frames of a certain duration, which are then divided into time slots. A single connection uses one time slot per frame. As a result, each frame that transmits includes a time slot for that connection. An advantage TDM has over FDM is that it does not require tuning to a certain frequency to get a transmission.
## R13 Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in ​Section 1.3​.)
### ​a. When circuit switching is used, how many users can be supported? 
The link can support 2 users because 2 x 1 Mbps is 2 Mbps.
### b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?
There will essentially be no queuing delay if two or fewer users transmit at the same time because the link can handle up to 2Mbps. If two users are transmitting at 1 Mbps, they will reach the output capacity of the link, but will not outgrow it. However, if three users transmit at the same time, there is a possible queuing delay when data is sent at the same time because it will be 3Mbps, greater than the capacity of 2 Mbps.
### c. Find the probability that a given user is transmitting.
There is a .2 probability at a user is transmitting.
### d. Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.
The probability that all three users are transmitting simultaenously is .2*.2*.2 = .008. The fraction of time in which the queue grows is .008 (1/125).
## R14 Why will two ISPs at the same level of the hierarchy often peer with each other? How does an ISP earn money?
ISPs of the same tier often peer with each other because it saves money. Peering allows an ISP to share traffic with another ISP of the same tier. Since higher tier ISPs often charge lower tier ISPs based on the amount of traffic they share, lowering the amount of traffic through peering saves money for lower tier ISPs. 

ISPs earn money through charging their customers, usually reflecting the amount of traffic that is shared. Higher tier ISPs are the providers of lower tier customer ISPs. 
## R18 How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 * 10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?
![r18](<r18.png>)
Yes, this delay depends on packet length because for the last bit to propogate, it has to wait until all the bits before it have propogated. This delay does depend on transmission rate because it determines the time before the last bit is encoded on the wire.
## R19 Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.
### a. Assuming no other traffic in the network, what is the throughput for the file transfer?
The throughput for the file transfer is 500 kbps because it is the slowest of the 3 rates.
### b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?
![r19b](<r19b.png>)
### c. Repeat (a) and (b), but now with  reduced to 100 kbps.
![r19c](<r19c.png>)