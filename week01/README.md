# Week 1: The Internet

This week we learned about the general structure of the Internet including physical technologies, packet-switching, and ISPs.

- [Chapter Questions](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week1/chapter-questions.md?ref_type=heads)
- [Learning Objectives](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week1/learning-objectives.md?ref_type=heads)
- [Wireshark Intro Lab](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week1/wireshark-lab.md?ref_type=heads)