# Wireshark Introduction
## Introduction
In this lab, we became familiar with using Wireshark by making packet captures and general observations of real traffic.
## Citations
I used [LCDR Downs' Portfolio](https://gitlab.com/jldowns-usna/ic322-downs-portfolio/-/blob/main/week01/wireshark.md?ref_type=heads) as a reference for formatting this Lab Writeup.
## Process
I used the getting started lab instructions from the [textbook website](http://gaia.cs.umass.edu/kurose_ross/wireshark.php).
## Answers to Lab Questions
### 1. Which of the following protocols are shown as appearing (i.e., are listed in the Wireshark “protocol” column) in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?
All of these protocols, except for quic, show least one entry when I type them in the search bar.
### 2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received? (By default, the value of the Time column in the packet-listing window is the amount of time, in seconds, since Wireshark tracing began.  (If you want to display the Time field in time-of-day format, select the Wireshark View pull down menu, then select Time Display Format, then select Time-of-day.))
There were about .023 seconds (23 milliseconds) between the GET message and the OK message.
![Time](<wireshark-time-capture.jpg>)
### 3. What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)?  What is the Internet address of your computer or (if you are using the trace file) the computer that sent the HTTP GET message?
The Internet address of gaia.cs.umass.edu is 128.119.245.12. My Internet address is 10.25.148.106.
### 4.	Expand the information on the HTTP message in the Wireshark “Details of selected packet” window (see Figure 3 above) so you can see the fields in the HTTP GET request message. What type of Web browser issued the HTTP request?  The answer is shown at the right end of the information following the “User-Agent:” field in the expanded HTTP message display. [This field value in the HTTP message is how a web server learns what type of browser you are using.] 
I used Chrome, however my User-Agnet field included four "browser" options. I read [this](https://humanwhocodes.com/blog/2010/01/12/history-of-the-user-agent-string/) article to figure out why it displayed the way that it did. I learned that Chrome uses Webkit as its rendering browser. Since Safari also uses the Webkit as a rendering browser, it sometimes shows up in the user-agent even if Chrome is being used. Mozilla is common to appear in all browsers because it was one of the original browsers created by Netscape. Many browsers try to conceal their true browser version by adding Mozilla, since Netscape was once the most dominant browser.
### 5.	Expand the information on the Transmission Control Protocol for this packet in the Wireshark “Details of selected packet” window (see Figure 3 in the lab writeup) so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent?
The destination port is 80. This port is known as HTTP's port.
### 6.	Print the two HTTP messages (GET and OK) referred to in question 2 above. To do so, select Print from the Wireshark File command menu, and select the “Selected Packet Only” and “Print as displayed” radial buttons, and then click OK.
![Get ok image 1](<wireshark-get-ok-1.jpg>)
![Get ok image 2](<wireshark-get-ok-2.jpg>)
