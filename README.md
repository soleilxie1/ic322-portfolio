# Soleil Xie IC322 Portfolio
I'm using this as a Portfolio for IC322: Computer Networks.

- [Week 1: The Internet](week01)
- [Week 2: The Application Layer - HTTP](week02)
- [Week 3: The Application Layer - DNS and Other Protocols](week03)
- [Week 4: The Transport Layer - TCP and UDP](week04)
- [Week 5: The Transport Layer - Part 2](week05)
- [Week 7: The Network Layer - Data Plane](week07)
- [Week 8: More Network Layer - More Data Plane](week08)
- [Week 9: The Network Layer - Control Plane](week09)
- [Week 10: The Network Layer - More Control Plane](week10)
- [Week 12: The Link Layer](week12)
- [Week 13: More Link Layer](week13)
- [Week 15: TLS Transport Layer Security](week15)