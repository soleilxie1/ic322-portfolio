# Week 7: Chapter Questions
## R11 Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).
Packet loss may occur at input ports when the packet switching occurs slower than packets arriving at the input ports. Any packets not being processed will be placed in a queue. Packets can only be transfered at the same time if they are going to different output ports. If the queue is too long for the buffer to contain, packets will have to be dropped since the buffer will not be able to store any more incoming packets. Packet loss at input ports can be eliminated by increasing the switch fabric speed. This will allow packets to be processed faster by the router and send the packets to the output ports before a queue is created in the input ports. 
## R12 Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?
Packet loss can occur at output ports when the rate at which the switch transfers packets than the output ports can can transmit packets out of the router. As a result, packet queues are formed behind output ports that transmit at a lower rate than the rate at which the switch sends packets to that port. If there is not enough buffer space for the packets in queue, packets must be dropped since there will be no more space to store incoming packets. This loss cannot be prevented by increasing the switch fabric speed. Doing this would only increase packet loss at output ports because the speed of transferring packets to the output ports would be even greater than the output port transmission speed.
## R13 What is HOL blocking? Does it occur in input ports or output ports?
HOL (head-of-the-line) blocking occurs in input ports. This happens when a packet is in queue and cannot be transferred to an output port even if that output port is free due to it being blocked by another packet ahead of the current packet in the queue. This happens when the interface the first packet in queue needs to go is not available yet, causing the blocking for the first packet, and then causing the blocking of the second packet. This can cause major packet losses because the switching to output ports is not completely optimized. 
## R16 What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?
RR (round robin) packet scheduling sorts packets into classes with priority queueing. There is not strict priority between classes since the foundation of RR scheduling is that it alternates services between classes. When work-conserving queuing is implemented with RR, the next class will be serviced if there are no available packets of the current serviced class. WFQ (weighted fair queuing) scheduling serves classes circularly: class 1, class 2, class 3, then class 1 again repeating the pattern. The main difference between RR and WFQ is that each class may receive service for a differing amount of time compared to other classes. There is a case where RR and WFQ will behave exactly the same. If the interval of time for each class is the same as the amount of time it takes to process a single packet, only one packet from each class will be processed before moving onto the next class.
## R18 What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?
The `Time-to-live` header ensures that a packet is not fowarded forever. Every time the packet is processed by a router, the value in this field decreases. If we wanted to ensure that a specific packet was forwarded through no more than N routers, `Time-to-live` should be set to N initially since after N routers have processed the packet, the packet will be dropped.
## R21 Do routers have IP addresses? If so, how many?
Routers have IP addresses, but these IP addresses are not typically associated with the individual router. Instead, IP addresses are associated with different interfaces on the router that is connected to a host. An interface on a router is the boundary that connects the host and its physical link to the router. The number of IP addresses a router has depends on how many active interfaces it has.
## P4 Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?
![p4](<p4.png>)

The best case scenario would require 2 time slots: 

1. x and y and z
2. x and y

The worst case scenario would require 3 time slots as well:

1. x and y
2. z and y
3. x
## P5 Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).
A sequence that might be served in order to achieve the WFQ weights is: **112311231123**

Because class 1 is 2x as much weight as class 2 and 3, there is twice as much time allotted for packets in class 1 than the other classes.
### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
A sequence could be: **112112112**

This is because there will be 2x the time for class 1 compared to class 2.
## P8 Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.
| Prefix Match      | Interface |
| :---------------: | :-------: |
| 11100000 00       | 0         |  
| 11100000 01000000 | 1         |
| 11100000          | 2         |
| 11100001 0        | 2         |
| otherwise         | 3         |
### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:
**11001000 10010001 01010001 01010101:** This address would go in Interface 3 because its prefix does not match any of the other interfaces.

**11100001 01000000 11000011 00111100:** This address would go in Interface 2 because it is its longest prefix matching interface.

**11100001 10000000 00010001 01110111:** This address would go in Interface 3 because its prefix does not match any of the other interfaces.
## P9 Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table: (For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.)

| Prefix Match | Interface |
| :----------: | :-------: |
| 00           | 0         |  
| 010          | 1         |
| 011          | 2         |
| 10           | 2         |
| 11           | 3         |

**Interface 0**: 00000000 to 00111111 (64 addresses)

**Interface 1**: 01000000 to 01011111 (32 addresses)

**Interface 2**: 01100000 to 10111111 (32 + 64 = 96 addresses)

**Interface 3**: 11000000 to 11111111 (64 addresses)

## P11 Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.
![p11](<p11.png>)