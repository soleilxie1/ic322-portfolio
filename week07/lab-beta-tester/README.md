# Lab: Beta Tester
## Introduction
In this lab, I beta tested LCDR Downs' new network simulator. I completed Chapters 1 and 2 as specified in the instructions.
## Citations
No other outside resources were used for this lab.
## Process
I followed the instructions specificied in the [gitlab](https://gitlab.com/jldowns-usna/protocol-pioneer).
## Questions
### 1. How did you solve Chapter 1? Please copy and paste your winning strategy, and also explain it in English.
```
self.send_message("Message Received", "W")
```

I changed this one line because I noticed when I ran it initially, there was a packet coming from "W" that asked us to respond with "Message Received" if we could receive their message. 
### 2. How did you solve Chapter 2? Again, copy and paste your winning strategy and explain it.
```
while(self.message_queue):
        m = self.message_queue.pop()
        print(f"Msg on interface {m.interface}: {m.text}")
        self.state["received"][m.interface][0] += 1
        self.state["received"][m.interface][1] += int(m.text)

    for interface in {"N", "S", "E", "W"}:
        if(self.state["received"][interface][0] == 3):
            self.send_message(str(self.state["received"][interface][1]), interface)
```

I kept track of how many messages came from each interface and I summed each interface's message. I then checked each interface to see if there were 3 messages received. If there were, I sent the total sum out that interface.

### 3. Include a section on Beta Testing Notes.
During Chapter 2, I sent the message to the Ratchets as a integer instead of string and it did not work. I got the message "received 20 expected 20". However, when I cast the message to a string, it worked.

During Chapter 2, I sent 3 packets to Ratchet 02 before Ratchets 01 and 03. However, even though Ratchets 01 and 03 were not confirmed as successful yet, it still gave me the victory screen because Ratchet 02 received 3 packets that were all successful.