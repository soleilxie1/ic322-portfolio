# Week 7: The Network Layer - Data Plane

This week, we learned about the Network layer, specifically what a subnet is and how they are used to divide parts of a network.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Beta Tester](lab-beta-tester)