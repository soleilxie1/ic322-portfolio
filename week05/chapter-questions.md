# Week 5 - Chapter Questions
## R17 Supp​ose two TCP connections are present over some bottleneck link of rate R bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?
TCP would like to give R/2 transmission rate for each connection, but this may fluctuate because of TCP congestion control. One connection may use more than the other during certain periods of time.
## R18 True or fa​lse? Consider congestion control in TCP. When the timer expires at the sender, the value of cwnd is set to one half of its previous value.
False, if the timer expires, the value of cwnd is set to 1 MSS (maximum segment size), the original starting value of the slow start phase.
## P27 Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.
### a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?
The second segment has the sequence number of 207, source port number of 302 and destination port number of 80.
### b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?
The acknowledgment number would be 207, since this is the segment number it expects to receive next. The source port number is 80 and the destination port is 302.
### c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?
The acknowledgment number would be 127, since it expects to receive the first segment first, even if the second segment arrived before the first. 
### d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.
![p27d](<p27d.png>)
## P33 In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?
TCP might avoid measuring the SampleRTT for retransmitted segments because if the segment had to be retransmitted, it means it was not correctly transmitted the first time. If it was not correctly transmitted the first time, there could be an underlying issue that causes it to not be correctly transmitted the second time. As a result, this would fluctuate the SaampleRTT. Moreover, a retransmitted packet would not have a genuine SampleRTT, because it already had a SampleRTT the first time it was transmitted, even if it failed.
## P36 In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?
The TCP designers might have chose to perform a fast retransmit after 3 duplicate ACKs and not 1 duplicate ACK since the receiver could be receiving the segments out of order. It is possible that the segment in question has not been lost, but is slower in transition than its surrounding segments. However, after 3 duplicate ACKs, it is likely that the segment was lost or corrupt.
## P40 Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.
![p40](<p40.jpg>) 
### a. Identify the intervals of time when TCP slow start is operating. 
From t = 0 to 6 and after t = 23, it seems that TCP is in the slow start phase since the congestion window is increasing exponentially.
### b. Identify the intervals of time when TCP congestion avoidance is operating. 
Congestion avoidance happens from t = 6 to 16 and t = 17 to 22 because the congestion window is increasing linearly.
### c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout? 
The segment loss was detected likely by a triple duplicate ACK because the congestion window is halved instead of going back down to zero. When triple duplicate ACKs are received, we know that the receiver is still receiving packets, but missed one in the middle.
### d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout? 
The segment loss was likely detected by a timeout because the congestion window drops all the way down to zero. This is because a timeout typically means an increase in congestion because the receiver has not yet sent three duplicate ACKs yet.
### e. What is the initial value of ssthresh at the first transmission round? 
The initial value of ssthresh at the first transmission round is 32 since this is about when the graph starts to go linear.
### f. What is the value of ssthresh at the 18th transmission round? 
The value of ssthresh at the 18th transmission round is 21 because this is half of the congestion window when the last packet was dropped.
### g. What is the value of ssthresh at the 24th transmission round? 
The value of ssthresh at the 24th transmission round is 14 because this is half of the congestion window when the last packet was dropped.
### h. During what transmission round is the 70th segment sent? 
Around transmission round 7 is when the 70th segment is sent because the integral between 0 and 7 with respect to the congestion window is about 70.
### i. Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ssthresh? 
The value of sshthresh will be 4 since this is half of the previous congestion window before the packet was dropped. The congestion window size would be 5 since a triple duplicate ACK sets the cwnd back by half of the previous cwnd.
### j. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 16th round. What are the ssthresh and the congestion window size at the 19th round? 
The congestion window would go back to zero, so the size at 19th round would be 4 and the ssthresh would be 21, half of the previous congestion window before the packete was dropped.
### k. Again suppose TCP Tahoe is used, and there is a timeout event at 22nd round. How many packets have been sent out from 17th round till 22nd round, inclusive?
22 - 17 = 5, 5 + 1 = 6
1 + 2 + 4 + 8 + 16 + 32 = 63 packets
The congestion window increases by a factor of 2 each time.