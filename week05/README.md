# Week 5: The Transport Layer - Part 2

This week, we learned more about the Transport layer, specifically flow control and congestion control.

- [Learning Objectives](learning-objectives.md)
- [Chapter Questions](chapter-questions.md)
- [Partner Feedback](https://gitlab.usna.edu/BenDab23/ic322-portfolio/-/issues/9)