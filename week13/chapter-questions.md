# Week 13 Chapter Questions
# Chapter 6
## R10 Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
C's adapter will check the frames to see if the MAC address matches C's adapter MAC address. Since it doesn't, it drops the frame. However, if A sends frames with the MAC broadcast address, C's adapter will pass the datagrams from the frames to the network layer. This is because the destination MAC address is addressed to C.
## R11 Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
The purpose of an ARP query is to figure out whose MAC address matches the IP address in the query. There needs to be a broadcast since we don't know which node has that IP address or the destination MAC address.
## P14 Consider three LANs interconnected by two routers, as shown in Figure 6.33.
### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
A: 192.168.1.25

B: 192.168.1.5

C: 192.168.2.25

D: 192.168.2.5

E: 192.168.3.25

F: 192.168.3.5
### b. Assign MAC addresses to all of the adapters.
A: A0-00-00-00-00-00

B: B0-00-00-00-00-00

C: C0-00-00-00-00-00

D: D0-00-00-00-00-00

E: E0-00-00-00-00-00

F: F0-00-00-00-00-00
### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
1. E creates IP datagram with destination B
2. E creates linnk layer frame containing datagram from step 1 with destination MAC address R2
3. R2 receives rame, sends to MAC address of R1
4. R1 removes datagram, deterines outgoing interface based on desitnation IP, sets destination MAC to B
5. R1 creates link layer frame and sends to B
6. B receives the frame
### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).
1. E creates IP datagram with destination B
2. E broadcasts, asking for gateway IP address
3. S3 broadcasts to relay E's message
4. R2 is gateway, responds with its MAC
5. S3 forwards to E
6. E sends datagram with destination IP of B, destination MAC of R2
7. E creates linnk layer frame containing datagram from step 1 with destination MAC address R2
8. R2 receives rame, sends to MAC address of R1
9. R2 creates IP datagram with destination B
10. R2 broadcasts, asking for gateway IP address
11. S2 broadcasts to relay R2's message
12. R1 is gateway, responds with its MAC
13. S2 forwards to R2
14. R2 sends datagram with destination IP of B, destination MAC of R1
15. R1 removes datagram, deterines outgoing interface based on destination IP, sets destination MAC to B
16. R1 creates IP datagram with destination B
17. R1 broadcasts, asking for gateway IP address
18. S1 broadcasts to relay R1's message
19. B is gateway, responds with its MAC
20. S1 forwards to R1
21. R1 creates link layer frame and sends to B
22. B receives the frame
## P15 Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.
### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?

No, because hosts E and F are in the same subnet.

Source: E's IP and MAC
Destination: F's IP and MAC

### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP an

No, however Host E will send an ARP query to R1.

Source: E's IP and MAC
Destination: B's IP and R1's MAC

### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?

S1 will forward the ARP query message out of both its interfaces and S1 will learn where host A to its forwarding table. R1 will receive the ARP query mesage. R1 will not forward the query message. Host B will not need to send an ARP query message as it retrieves Host A's MAC address from the original ARP query message. S1 adds Host B to its forwarding table and drop the ARP response message.

# Chapter 7
## R3 What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?

**Path Loss:** Signal may disperse if the distance across the channel increases
**Multipath Propogation:** Electromagnetic waves can reflect off of other objects and the ground. This causes a blurred signal on the receiver's side.
**Interference From Other Sources:** Radio sources that transmit in the same frequency can interfere with one another. Additionally, electromagnetic noise in the environment can also cause interference (microwaves, motors).

## R4 As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?    

A base sattion can increase the power of its transmission. It can also decrease its transmission rate.

## P6 In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
The second frame may not be sent immediately in case the entire message is very long and sending all frames back-to-back would hog the channel, preventing other nodes from sending other frames.

## P7  Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.
Time it takes to transmit RTS frame + Time it takes for AP transmit CTS frame + Time it takes for host to transmit the data