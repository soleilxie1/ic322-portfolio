# Vint Cert Mania
## Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
He helped found arpanet in the 1970s, which is the foundation of the Internet. He also helped designed TCP/IP protocols.
## Find 3 surprising Vint Cerf facts.
- Vint Cerf likes to appear in 3-piece suits.
- Vint Cerf works with humanitarian organizations.
- Vint Cerf is also working on the interplanetary Internet with NASA.
## Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
- What did you envision the Internet would be like when it was being developed? (I think this question is interesting because it reveals what he expected the Internet to be like versus how it is today.)
- What was your biggest regret throughout your career? (I think this question is interesting because he may reveal what he wanted to do during his career that he was unable to do.)