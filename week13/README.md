# Week 13: More Link Layer

This week we learned more about the link layer including 802.11 and CSMA/CA.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Vint Cerf Mania](vint-cerf.md)
- [Lab: Protocol Pioneer](lab-protocol-pioneer)
- [Community Garden Feedback](https://gitlab.com/mcwieland/ic322-portfolio/-/issues/26)