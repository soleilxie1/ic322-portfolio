# Lab: Protocol Pioneer
## Introduction
I completed Chapter 1 of Act II of Protocol Pioneer.
## Citation
I was not here for class, but I discussed solutions with Michael Wieland and used his solution to make my own.
## Process
I followed the directions given to us by LCDR Downs.
## Questions
### 1. Include your client and server strategy code.
```
from lib.Act02Chapter01 import Act02Chapter01
# Welcome to the StellarScript Console!

def client_strategy(self):

    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Client {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)
    tick_section = self.current_tick() % 40

    send = False
    if self.id == "1" and tick_section < 5:
        send = True
    elif self.id == "2" and tick_section >= 10 and tick_section < 15:
        send = True
    elif self.id == "3" and tick_section >= 20 and tick_section < 25:
        send = True
    elif self.id == "4" and tick_section >= 30 and tick_section < 35:
        send = True

    if send == False:
        print(f"{self.id}: Waiting.")
        return

    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    if not self.interface_sending(selected_interface):
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            print(f"{self.id}: Attempting send.")
            self.send_message(msg_text, selected_interface)




########################
## Run the simulation ##
########################
s = Act02Chapter01(
            client_strategy,
            client_strategy,
             animation_frames=1,
             wait_for_user=False

            )
s.run()
```
### 2. Explain your strategies in English.
I gave each client/server a time slot of 10 ticks. They have the first 5 ticks to start sending the message and the last 5 ticks to finish sending the message. I combined the client and server strategies because they ultimately do the same thing: send and receive messages.

### 3. What was your maximum steady-state success rate (after 300 or so ticks?)
My maximum steady-state sucess rate was between 0.135 and 0.140.

### 4. Evaluate your strategy. Is it good? Why or why not?
I think it is pretty good for a simple strategy. I used simple time division multiplexing to achieve a higher success rate than the original solution.

### 5.Are there any strategies you's like to implement, but you don't know how?
I wanted to implement something similar to CSMA/CD where a client could listen first for anything being transmitted before 

### 6. Any other comments?
N/A