# Week 12: Link Layer

This week we learned about the link layer, specifically the multiple access problem and error-checking.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Protocol Pioneer](lab-protocol-pioneer)