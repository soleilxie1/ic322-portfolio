# Week 12 Chapter Questions

## R4 Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as Dprop Will there be a collision if Dprop < L/R Why or why not?

$d_{prop}$ is L/R because this is the amount of time it takes for one node to propogate a single packet. There will be a collision if $d_{prop}$ < L/R because the first packet has not fully propogated across the channel yet.

## R5 In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?

**The four desirable characteristics:**
1. When only one node has data to send, that node has a throughput of R bps.
2. When M nodes have data to send, each of these nodes has a throughput of R/M bps. This need not necessarily imply that each of the M nodes always has an instantaneous rate of R/M, but rather that each node should have an average transmission rate of R/M over some suitably defined interval of time.
3. The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
4. The protocol is simple, so that it is inexpensive to implement.

*(Source: Computer Networking, James Kurose)*

Slotted ALOHA seems to have all of these characteristics. Slotted ALOHA divides its broadcast channel into time slots of L/R seconds. If only one node has to send a frame, it will send at the next available slot at R bps always. There is an average of R/M bps throughput when multiple nodes since each node will attempt to transmit at the beginning of each time slot unless there is already a packet being transmitted. There is not a master node in slotted ALOHA and it seems to be simple and easy to implement.

Token Passing implements all of these characteristics except for it being decentralized. This is because if one node fails to pass the token correctly, it could cause the entire channel to fail.

## R6  In CSMA/CD, after the fifth collision, what is the probability that a node chooses K=4? The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?

**Probability of K = 4:** $\frac{1}{2^n} = \frac{1}{32}$

**Number of Seconds on 10 Mbps:** $\frac{K * 512}{2^{20}}$ = 0.001953 seconds

## P1 Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.

![Alt text](p1.png)

## P3 Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.

![Alt text](p3.png)

## P6 Consider the previous problem, but suppose that D has the value

### a. 1000100101.

![Alt text](p6a.png)

### b. 0101101010.

![Alt text](p6b.png)

### c. 0110100011.

![Alt text](p6c.png)

## P11 Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
### a. What is the probability that node A succeeds for the first time in slot 4?

$$(1-p)^3 * p * (1-p)^3$$

### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?

$$p * 4 * 1/4$$

### c. What is the probability that the first success occurs in slot 4?

$$4 * ((1-p)^3)^4 * p$$

#### d. What is the efficiency of this four-node system?

$$4p(1-p)^3$$

## P13  Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is d_poll Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?

$$\frac{Q}{\frac{R}{Q} + d_{poll}}$$