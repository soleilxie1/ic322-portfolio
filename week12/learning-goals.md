# Week 12 Learning Goals

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.

The multiple-access problem is figuring out how to have multiple nodes send and receive data across the same channel. ALOHA and CSMA/CD are random access protocols, a class of multiple access protocols where a transmitting node transmits at the full channel rate until a collision occurs. Once the collision occurs, the node waits for a random amount of time before retransmitting the frame. The node will keep attempting to retransmit the frame until it is successful.

### ALOHA

In slotted ALOHA, all frames and L bits long. Time is divided into L/R sized slots (the time it takes to transmit one frame). Nodes will only begin transmitting at the beginning of a slot. A collision will always be detected before the slot ends because both nodes would have had to start transmitting at the beginning of the slot. If a collision happens, the node will attempt to retransmit the frame in the following spots with probability p. The unslotted version of ALOHA was the original one, where the nodes are not synchronized and do not send their frames based on time slots. Instead, if a collision occurs, the node will retrasmit the frame immediately after receiving notification of the collision with probability p. If it does not retransmit, the node will wait for a time it takes for a frame to be transmitted, and then attempt to transmit the frame again with probability p. Slotted ALOHA is more efficient than unslotted ALOHA with an efficiency of 0.37, compared to 0.185. Efficiency is measured by the ratio between long-run successful slots to unsuccessful slots when there are a large number of nodes sending a large number of frames.

### CSMA (Carrier Sense Multiple Access) / with Collision Detection

CSMA attempts to optimize the amount of data exchanged while minimizing the amount of time spent alleviating and experiencing collisions. The book compares this to humans with more "civility" where protocols cause nodes to *listen before speaking* and *stop talking if someone else is talking*. **Carrier sensing** is used to detect if another node is current transmitting a frame. Until the node hears no more transitions, it is time for that node to talk and begins to transmit its frame after waiting a short amount of time. If a collision is detected, the node will stop its transmission, knowing to stop talking if someone else is talking, and wait a random amount of time before going back to carrier sensing. Even though carrier sensing may exist, there is still a big possibility of collisions happening even if nodes listen before they talk. This is because of propogation delay, where the transmitting frame has not reached a certain part of the channel, causing that node at the end of the channel to believe there is no one talking.

## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.

### Parity Bits

The use of Parity Bits is one of the simplest forms of error detection. The sender will send an additional bit along with the original data, known as the parity bit. Usually, an even parity scheme is used. In this case, if there are an even number of 1s in the data, the parity bit is set to 0 so the number of 1s is still even. If there are an odd number of 1s in the data, the parity bit is set to 1 to create an even number of bits. When the receiver counts the numbe of 1s in the data along with the parity bit, an odd number of 1s indicates that there is an error in the data. The same can be done for an odd parity scheme where an even number of ones means there is a flipped bit. If there are an even number of bit errors, this will cause all the errors to go undetected, hence why parity bits are the simplest forms of error detection.

### 2D Parity

The 2D parity scheme is similar to the single-bit parity scheme in that it still uses the idea of parity bits, but incorporates this by dividing data into a matrix of i rows and j columns. Each row and column has its own parity bit. There is also an additional parity bit included that is the parity bit for the entire data. To check for errors, we can look at each column and row to check for odd numbers of ones like the single-bit parity scheme. However, this time the bit error can actually be detected by cross-referencing other rows/columns. 

![Alt text](2dparity.png)

In this example, the specified row and column both have parity errors. This allows us to detect the actual bit causing the error.

### Internet Checksum

The Internet checksum is calculated by dividing data into k-sized bits of data and summed. The 1s complement of this sum is calculated and carried in the segment header. When the receiver receives the packet, it checks the checksum by calculating the 1s complement of the data (with the checksum). If the sum is 0, there are no errors. If there is a 1, an error is prsenet. Checksumming is typically used at the transport layer because it is a simple and fast way to detect errors.

### CRC (Cyclic Redundancy Check)

Also known as polynomial codes, CRC is used as an error-detection technique at the link layer. There are more steps involvedfor CRC, so faster operations that are present in dedicated hardware are required. The sender and receiver agree on a r + 1 bit pattern (G). The sender will choose an additional r bits to append to the original data d, such that d + r is divisible by G. When the receiver receives the data, it divides d + r by G. If there is a remainder, this indicates an error.

![Alt text](crc.png)

In this example, there seems to be an error because dividing the d + r by G produces a remainder.

## I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.

Ethernet is a wired LAN technology and is the most widely used across the world. LANs (Local Area Networks) operate in the Link Layer instead of the Network Layer. As a result, link-layer frames are sent via switches (link layer packet switches). Instead of IP addresses, MAC addresses are used. Unlike IP addresses, MAC addresses are unique and no adapters will have the same address.

Earliest Ethernet technologies used a hub-based topology. The hub is a device that acts on the physical layer. It simply sends out every bit it receives from one interface out of the other interfaces. Ethernet now uses switches, which uses store-and-forward packet switching and operates at the link layer. Ethernet provides a connectionless service to the network layer by encapsulating a layer 3 datagram into a layer 2 frame and sends it across the LAN without having to confirm connection with the destination. This is similar to UDP's connectionless server at layer 4.

Different Ethernet versions can be identified by their acronym like 10GBASE-T. The numbers at the beginning refer to the speed. In this case, it is 10 Gigabit per second. BASE means that there will only be Ethernet traffic carried across the medium. The last part refers to the type of material of the physical media. In this case, "T" is twisted-pair copper wire. 