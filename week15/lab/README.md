# Amazon AWS DDoS Attack 2020
## Background
In February of 2020, Amazon experienced a DDos (Distributed Denial of Service) attack which fired 2.3Tbs. DDoS attacks are when an attacker attempts to flood a website by sending too many requests at once to crash the website. This attack surpassed the previous record from 2018 where 1.7 Tbs were fired. Fortunately, Amazon's AWS shield was able to protect customers from experiencing this attack. As a result, the only evidence of this attack happening is Amazon's statement. This specific attack is reported to have been a "reflection attack", meaning a third-party server was used to increase the amount of data being sent. The servers being exploited in this attack were CLDAP (Connectionless Lightweight Directory Access Protocol) servers. There have been several other attacks that have used this protocol to edit directories shared on the internet. 

![Alt text](graph.png)

*Source: Amazon*

The graph above shows that this attack had the greatest amount of traffic volume over the interval of 3 months.
## DDoS
DDoS attacks happen when high amount of Interet traffic are used to flood a server, service, or network. This is commonly done by creating a group of bots that can be controlled by the attacker. As each bot continously sends requests, this can flood the server, service or network, causing normal hosts to experience a denial of service. In Amazon's case, they experienced a CLDAP attack where the attackers exploited the protocol's weaknesses to flood traffic across the AWS network. CLDAP is a UDP protocol which allows users to access files and directories over public and private networks. There are generally three stages to a CLDAP reflection attack:

**1. Reconnaissance:** Attackers search the internet for vulnerable servers. These servers are likely to respond to CLDAP requests.

**2. Spoofing:** The attackers send requests, likely from bots, to the vulnerable servers and make them appear to come from the target network (AWS, in this case).

**3: Amplification:** The servers respond to these requests and send larger amounts of data to the target network, causing flooding to the target network. This reduces the need for the attackers to actively rely on bots to send data. Instead, the vulnerable servers will amplify the amount of data sent due to the amplification factor.

Other types of DDoS include Application Layer attacks, Protocol Attacks, and Volumetric Attacks.

**Application Layer Attack**

This attack is done throught the Application layer and is aimed to cause a denial-of-service, consuming the victim's resources. This can be done by repeating HTTP requests, which are cheap for a client to execute, but extremely expensive for servers to respond. This is because the server has to retrieve files and other web pages from a GET request. These attacks are more difficult to defend since it can be hard to recognize who is a legitimate target and who is an attacker. This specific attack is known as *HTTP Flood*, while there are many other attacks that can happen on the application layer.

**Protocol Attack**

The goal of a Protocol attack is to exhaust network equipment resources such as firewalls and load balancers. The primary objective is to make the victim inaccessible to other end devices. A specific example of this type of attack is *SYN Flood*. This happens when the victim receives too many SYN requests that originate from spoofed IP addresses. The attacker is able to exploit the TCP handshake by repeating the SYN step, attempting to connect but never actually making the connection. As a result, the victim's resources are consumed and is unable to respond to legitimate clients.

**Volumetric Attack**

The goal of this attack is to exhaust all the available bandwidth from a victim and the Internet. Amplificationis used to create large amount of traffic, likely using bots. The bots can use DNS spoofing to make requests to a DNS server with the victim's address. The DNS server responds to each request, flooding the victim's bandwidth.

## Mitigating DDoS Attack

The key to mitigating a DDoS attack is being able to tell which traffic originates from the attacker and which traffic is legitimate. If the attacker is able to conceal itself as normal traffic, it will be hard to discern normal from attacker traffic. Some mitigation techniques involve blackhole routing, rate limiting, web application firewall, and anycast network diffusion.

**Blackhole Routing**

This solution involves sending all traffic (both from attacker and legitimate) to a "black hole". As a result, this may not be the best solution since normal traffic will not be filtered out and instead also reach the black hole. This solution is best used as a last resort and normal traffic can be sacrificed.

**Rate Limiting**

Rate limiting involves setting a maximum rate a requests a server can accept. This is useful in specific cases, such as brute-force login and web scrapers. Attackers will not be able to amplify the amount of traffic being sent to a server.

**Web Application Firewall**

This method is helpful when preventing an Application layer attack. The firewall can protect the victim from the attacker's traffic by being able to create custom rules when responding to an attack. This is likely what Amazon used to mitigate their attack, which is Amazon's AWS shield.

**Anycast Network Diffusion**

This solution involves using an anycast network to spread out the attacker's traffic across distributed servers. This is to increase the size of traffic a network to handle. If a network can handle receiving large amounts of traffic, the DDoS attack can be easily mitigated since the network can sustain the large amount of traffic flooding.

## Ethical/Legal Considerations 

### Ethical Implications

While DDoS attack don't necessary threaten privacy or data integrity, it affects availability. If a server is compromised because of a DDoS attack, it cannot receive normal traffic since it is being flooded by the attacker's extra traffic. Professionals in the field should take measures to prevent DDoS attacks. This can be done using many of the mitigation techniques mentioned above, including a firewall and anycast network diffusion.

### Legal Implications

DDoS attacks are illegal in the United States. This encourages professionals to work together to mitigate potential adversarial attacks. While these attacks may be illegal, they can still happen as cyber criminals are just as common as normal criminals.

### Professional Responsibilities

Computing professionals can mitigate DDoS attacks ethically by providing a good defense without causing more damage than needed. The mitigations discussed above all fall under these guidlines, with the exception blackhole routing, which can cause damage to hosts sending normal traffic. Computing professionals should focus on isolating the attacker without affecting anything else on the Internet.

## Sources
- [BBC](https://www.bbc.com/news/technology-53093611)
- [The Verge](https://www.theverge.com/2020/6/18/21295337/amazon-aws-biggest-ddos-attack-ever-2-3-tbps-shield-github-netscout-arbor)
- [CloudFlare](https://www.cloudflare.com/learning/ddos/what-is-a-ddos-attack/)
- [Akamai](https://www.akamai.com/glossary/what-is-a-cldap-reflection-ddos-attack)
