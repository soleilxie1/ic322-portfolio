# Week 6 - Chapter Questions
## R20 In the TLS record, there is a field for TLS sequence numbers. True or false?
False, the sequence numbers are included in the HMAC and are encrypted.
## R21  What is the purpose of the random nonces in the TLS handshake? 
The random nonces are used the create the session keys $E_B , M_B , E_A , M_A$.
## R22. Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
False, the IVs are obtained from the Master Secret key.
## R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
Bob will realize he is not communicating with Alice after he sends the Pre-Master Secret key since Trudy will not be able to decrypt this because Trudy does not have access to Alice's private key.
## P9  In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys , Sa and Sb, respectively. Alice then computes her public key, Ta, by raising g to Sa and then taking mod p. Bob similarly computes his own public key Tb by raising g to Sb and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising Tb to Sa and then taking mod p. Similarly, Bob calculates the shared key S' by raising Ta to Sb and then taking mod p. 

### a. Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S'.
Ta = $g^{S_a}  mod  p$

Tb = $g^{S_b}  mod  p$

S = $Tb^{S_a}  mod  p = g^{S_a * S_b}  mod  p$

S' = $Ta^{S_b}  mod  p = g^{S_b * S_a}  mod  p$
### b. With p=11 and g=2, suppose Alice and Bob choose private keys Sa = 5 and Sb = 12, respectively. Calculate Alice’s and Bob’s public keys, Ta and Tb. Show all work.
![Alt text](publickeys.png)
### c. Following up on part (b), now calculate S as the shared symmetric key. Show all work.
![Alt text](symmkey.png)
### d. P1rovide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.
![Alt text](trudy.png)
## P14 The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
MACs are configured on the device itself and no adapter can have the same MAC. MACs are configured in the hardware and are not created digitally.
## P23 Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
R2 will not forward the duplicate datagram into the branch office network. This is because R2 keeps track of the sequence numbers encrypted in the HMAC. If there is a duplicate number, R2 will not forward the datagram to the branch-office network since it already sent that sequence number's datagram.