# Week 15: Transport Layer Security

This week we learned about TLS and how to mitigate man-in-the-middle attacks

- [Learning Objectives](learning-objectives.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Security Spelunker](lab)
- [Community Garden](https://gitlab.usna.edu/m252244/ic322-portfolio/-/issues/26)