# Week 8: More Network Layer - More Data Plane

This week, we learned more about the network layer's data plane, specifically NAT and IPv6.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Beta Tester](lab-beta-tester)
- [Community Garden](https://gitlab.usna.edu/M1Chen1/ic322-portfolio/-/issues/15)