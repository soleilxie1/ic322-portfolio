# Week 8: Chapter Questions
## R23 Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
I visited my own personal computer that is connected to the USNA resnet.

- **IPv4 Address:** 172.30.141.149
- **Network Mask:** 255.255.192.0
- **Default Router:** 172.30.128.1
- **DNS Server:** 172.21.192.11
## R29 What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
A private network address is used in a specific private network in which those addresses are only used in that specific private network. For example, in small-office, home office (SOHO) networks, addresses used specifically in that network are only used in that private subnet. These private network addresses should never be present in the larger public network because other private subnets may be using the same group of addresses (commonly 10.0.0.0). Instead, private network addresses are mapped to public addresses in the NAT translation table which are used in the larger public Internet.
## R26 Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?
IP addresses are assigned to hosts in a wireless network using DHCP. The wireless router acts as a simple DHCP server that directly assigns hosts in the network IP configuration information. The wireless router uses NAT because even though it is wireless, it does not change the problem that NAT aims to solve. Private IP addresses can still be present in a wireless network and those addresses will only be used in that specific network. NAT gives the hosts in the network a public IP address so they can access the internet while taking up minimal address space on the larger internet.
## P15 Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.
![Alt text](p15.png)
### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y.
- **Subnet A:** 214.97.254.0/24
- **Subnet B:** 214.97.255.0/26 - 214.97.255.112/29
- **Subnet C:** 214.97.255.128/25
- **Subnet D:** 214.97.255.120/31
- **Subnet E:** 214.97.255.122/31
- **Subnet F:** 214.97.255.124/31
### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.
**R1:**

| Prefix Match      | Interface |
| :---------------: | :-------: |
| 11111110          | 0         |  
| 11111111 0111100  | 1         |
| 11111111 1        | 1         |
| 11111111 0        | 2         |

**R2:**

| Prefix Match      | Interface |
| :---------------: | :-------: |
| 11111111 1        | 0         |  
| 11111111 0111100  | 1         |
| 11111110          | 1         |  
| 11111111 0        | 2         |  

**R3:**

| Prefix Match      | Interface |
| :---------------: | :-------: |
| 11111111 0        | 0         |  
| 11111110          | 1         |  
| 11111111 011110   | 1         |
| 11111111 011111   | 2         |
| 11111111 1        | 2         | 

## P16 Use the whois service at the American Registry for Internet Numbers (http://www.arin.net/whois) to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.
**University of California, Riverside:**
Address Range: 2607:f290:: - 2607:f290:ffff:ffff:ffff:ffff:ffff:ffff

![Alt text](ucriverside.png)

**University of Washington:**
Address Range: 128.208.0.0 - 128.208.255.255

![Alt text](uw.png)

**University of British Columbia:**
Address Range: 2607:5940:: - 2607:5940:ffff:ffff:ffff:ffff:ffff:ffff

![Alt text](ubc.png)

Although all of these universities had accurate geographic information associated with their IP addresses, this might not always be the case with whois services. The IP address could be registered to a different address if the owner does not want their address displayed in the whois database.
## P18 Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.
![p18](p18.png)
### a. Assign addresses to all interfaces in the home network.
- Local Router Interface: 192.168.1.1
- Host A: 192.168.1.2
- Host B: 192.168.1.3
- Host C: 192.168.1.4
### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.
| WAN                | LAN              |
| :----------------: | :--------------: |
| 24.34.112.235/4001 | 192.168.1.2/5001 |  
| 24.34.112.235/4002 | 192.168.1.2/5002 |  
| 24.34.112.235/4003 | 192.168.1.3/5003 |
| 24.34.112.235/4004 | 192.168.1.3/5004 |
| 24.34.112.235/4005 | 192.168.1.4/5005 |
| 24.34.112.235/4006 | 192.168.1.4/5006 |
## P19 Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.
### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.
We can count the number of unqiue hosts behind a NAT by counting the number of identification numbers that are out of order. For example, if we see 8, 9, 10, 11, 12... we know that is from one host. However, if we see 8, 9, 50, 51, 10... we know that there are at least two hosts since there are two different sequences of identification numbers being sent.
### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.
No, my technique would not work because there would be no way to differentiate between hosts based on the identification number on the packets. If all the numbers are random, there is no way to associate them with a specific host.