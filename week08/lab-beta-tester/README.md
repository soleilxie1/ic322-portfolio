# Lab: Beta Tester
## Introduction
This week, I beta tested Protocol Pioneer chapters 3 and 4.
## Citation
No other outside resources were used.
## Process
I followed the instructions for setup as specified in the game's [gitlab](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads) and continued to follow instructions specified in the actual chapters.
## Question
###  1. How did you solve Chapter 3? Please copy and paste your winning strategy, and also explain it in English.
```
messages = m.text.split('\n');
value = messages[2].split(":")[1]

if str(self.id) == messages[0].split(":")[1]:
    if "start_emit" in messages[1]:
        self.start_emit(value)
    elif "focus" in messages[1]:
        self.focus(value)
    elif "return_results" in messages[1]:
        self.return_results(value)
elif self.id == "1":
    if "2" in messages[0] or "4" in messages[0] or "5" in messages[0]:
        self.send_message(m.text, "E")
    elif "3" in messages[0] or "6" in messages[0]:
        self.send_message(m.text, "N")
elif self.id == "2":
    if "5" in messages[0]:
        self.send_message(m.text, "S")
    elif "4" in messages[0]:
        self.send_message(m.text, "N")
elif self.id == "3":
    if "4" in messages[0] or "6" in messages[0]:
        self.send_message(m.text, "E")
elif self.id == "4":
    if "6" in messages[0]:
        self.send_message(m.text, "N")
```

![Alt text](chap3.png)

I looked at how the ships were connected and I used that to make conditions for where each ship should route an incoming message based on the message's destination. For example, at ship 1, if the destination was either 5, 2, or 4, the ship should route the message to its East interface.
### 2. How did you solve Chapter 4? Again, copy and paste your winning strategy and explain it.
```
def drone_strategy(self):
    """Drones are responsible for routing messages."""
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")

        dest = float(m.text.split("\n")[1].split(":")[1])
    
        address = float(self.id)
    
        right = False
        if address > 3.4:
            right = True
        
        if dest > 2:
            if right == False:
                if address == 3.4:
                    self.send_message(m.text, "N")
                else:
                    self.send_message(m.text, "E")
            else:
                if dest == 2.1:
                    self.send_message(m.text, "N")
                elif dest == 2.2:
                    self.send_message(m.text, "S")
        elif dest < 2:
            if right == True:
                self.send_message(m.text, "W")
            else:
                if dest == 1.1 or dest == 1.2:
                    if (dest == 1.1 and address == 3.1) or (dest == 1.2 and address == 3.2):
                        self.send_message(m.text, "W")
                    elif (dest - 1) > (address - 3):
                        self.send_message(m.text, "S")
                    else:
                        self.send_message(m.text, "N")
                elif dest == 1.3:
                    if address == 3.4:
                        self.send_message(m.text, "W")
                    else:
                        self.send_message(m.text, "S")
        
def scanner_strategy(self):
    """Scanners are responsible for receiving messages, parsing them, taking
    action, and responding with results."""
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Scanner {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")

        messages = m.text.split("\n")
    
        value = ""
        if "Boot" in messages[2]:
            value = self.boot(int(messages[3].split(":")[1]))
        elif "Aim" in messages[2]:
            value = self.aim(int(messages[3].split(":")[1]))
        elif "Scan" in messages[2]:
            value = self.scan(int(messages[3].split(":")[1]))
    
        iface = "S"
        if self.id == "2.2":
            iface = "N"
            
        self.send_message("Source:" + self.id + "\nDest:" + messages[0].split(":")[1] + "\nCommand:Result\nValue:" + str(value), iface)
```

For the scanners, I parsed the messages to find the input value and correct function to use. After finding the function, I saved the returned value and added it to the message the scanners would send. Based on the location of the scanners, they only had one direction to move (North or South) since they were at the very edge and only connected to one other device.

For the drones, I first checked the destination address. If the destination was on the opposite side of the drone, the drone would route the message toward the destinaton. If the destination was a scanner, the drones would send messages North or South depending on which scanner it was. If the destination was an analyzer, we would have to check specifically, since there was no explicit pattern for which devices were connected to which drones. 
### 3. Beta Testing Notes
The only main problem I saw when playing chapters 3 and 4 were that every time the frame refreshes, all the objects disappear and then reappear again.