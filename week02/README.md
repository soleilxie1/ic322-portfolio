# Week 2: The Application Layer - HTTP

This week we learned about the characterstics of HTTP including types of requests/responses, cookies, and CDNs.

- [Learning Objectives](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week2/learning-objectives.md?ref_type=heads)
- [Chapter Questions](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week2/chapter-questions.md?ref_type=heads)
- [Lab: Build a Server](lab-build-a-server)