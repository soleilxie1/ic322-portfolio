from socket import *
import sys

serverPort = 12001
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The server is ready to receive')
while True:
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(1024).decode()
    capitalizedSentence = sentence.upper()
    print(sentence)

    response = "HTTP/1.1 200 OK\r\n"
    filename = sentence.split()[1]
    if filename[len(filename) - 4:] == ".png": #encode stuff differently if it is a png
        response += "Content-type: image/png\r\n\r\n" # specify content type to support pngs
        f = open(filename[1:], "rb") # read it as binary, since it is an image file
        connectionSocket.send(response.encode())
        connectionSocket.send(f.read())
    elif filename[len(filename) - 4:] == ".jpg": #encode stuff differently if it is a jpg
        response += "Content-type: image/jpeg\r\n\r\n" #specify content type to support jpg
        f = open(filename[1:], "rb") # read it as binary, since it is an image file
        connectionSocket.send(response.encode())
        connectionSocket.send(f.read())
    elif (filename != "/favicon.ico"): #favicon.ico requests the icon for the browser, but we don't have one yet
        response += "\r\n"
        f = open(filename[1:])
        output = f.read()

        #send the response line first
        connectionSocket.send(response.encode())

        #now send the output
        for i in range(0, len(output)):
            connectionSocket.send(output[i].encode())
        connectionSocket.send("\r\n".encode())
    connectionSocket.close()