# Lab: Build a Server
## Introduction
In this lab, I learned how to build a web server that supports HTML documents and pictures like PNGs and JPEGs. 
## Collaboration
I used this forum post on [Binary Files](https://www.geeksforgeeks.org/reading-binary-files-in-python/) to learn how to read image files in binary. I also used this forum post on [Content-types](https://stackoverflow.com/questions/28567733/how-to-encode-image-to-send-over-python-http-server) to learn why and how the `Content-type` header should be changed. I also used the course textbook as a reference for building the initial server.
## Process
I followed the [instructions](https://ic322.com/#/assignments/week02/lab-build-a-server) on the course website. I used a few forum posts (cited in section above) to help with the last part of the lab.
## Questions
### Question 1: Why are we focusing on the TCP server in this lab rather than the UDP server?
We are focusing on the TCP server because it is connection-oriented and ensures that data sent will be received on the other end. In UDP, the destination address of each packet needs to be specified since a UDP server socket can be connected to several clients. TCP servers have one welcoming socket which is used to establish the connection between the server and client. It also has one socket for each individual connection with each client. As a result, there is no need for a destination address because each socket is only used for the connection with that specific client. Since we don't know what destination we will be sending information to yet, it is better to use the TCP server because it will establish a secure connection between the server and each client. HTTP always uses TCP so we are using TCP for this server.
### Question 2: Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: 'cr' and 'lf'. What are these characters and how do we represent them in our Python response string?
The characters `cr` and `lf` are used to indicate a terminated line. We can represent them with `\r` for `cr` and `\n` for `lf`.
### Question 3: When a client requests the "/index.html" file, where on your computer will your server look for that file?
It looks in the directory where the server is. However, if it were a different path (e.g dir/index.html) it would look at that directory relative to the server.
