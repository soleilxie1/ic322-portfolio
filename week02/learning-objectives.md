# Week 2 Learning Objectives
## I can explain the HTTP message format, including the common fields.
HTTP request message format:

```
GET /directory/page.html HTTP/1.1
Host: www.website.com
Connection: close
User-agent: Mozilla/5.0
Accept-language: en
```

The first line is known as the request line. It includes a method field (GET, POST, HEAD, PUT, DELETE), a URL field, and the HTTP version (1.1). The method field usually is GET, since the browser is most likely requesting something from the server. The next lines are header lines. The `Host` line is the domain name or IP address of the object being requested. The `Connection` line being closed means that the server will close the connection after sending the object. The `User-agent` line indicates what browser is being used. In this case, it is a Firefox browser. The `Accept-language` line specifies the preferred language of the object. Other headers can be added, but usually only the `Host:` header is required.

HTTP response message format:

```
HTTP/1.1 200 OK
Connection: close
Date: Wed, 30 Aug 2023 07:07:07 GMT
Server: Apache/2.2.3 (CentOS)
Last-Modified: Wed, 30 Aug 2023 07:07:06 GMT
Content-Length: 3829
Content-Type: text/html
(...)
```

The first line in this response message is the status line. It has three fields: protocol version field, status code, and a status message relaying the status code. The version is HTTP 1.1 and the status code is 200 (status message OK), meaning the server is sending the object normally. Different status messages may include `Moved Permanently`, `Bad request`, `Not Found`, `HTTP Version Not Supported`. The `Connection:` header line has status "close", meaning the connection will be closed after the object is sent. The `Date` header line is the time the HTTP response was created and sent. The `Server` header line reveals what browser was used to generate the HTTP request that invoked the reponse (like `User-agent` line in HTTP request). The `Last-Modified` header reveals when the object was last modified. The `Content-Length` header line is the number of bytes of the object. The `Content-Type` header line refers the header of the object. In this case, it is an HTML text file. 
## I can explain the difference between HTTP GET and POST requests and why you would use each.
The `GET` request is used when a browser requests an object that is specified in the URL field. This is usually a web page being requested from a server. The `POST` method is used when a user fills out a form. It is similar to the `GET` request in that it can also request an object or web page. Data sent through a `POST` request is often more secure because the inputs are not revealed in the URL like the `GET` request does. If there was a web server that had a user enter a username and password, it would not be preferred to use `GET` since the fields would be visible in the URL.
## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
`CONDITIONAL GET` requests are used when we want to verify that an object is up to date. This is often implemented when web caching is used because the cache needs to make sure that it is forwarding the updated version of the object to the browser. The request message is a regular `GET` method with an additional `If-Modified-Since:` header line. A web cache keeps the `Last-Modified` date from the previous HTTP request when it last received the object. It puts that same date in the `If-Modified-Since` line and sends that with the `GET` request method to the server to ensure it has the latest version. The server will send an HTTP reponse that either suggests there has been no modification since that date (304 Not Modified) or it will send the actual object. The Conditional GET solves the problem of a cache having a copy of an object that is outdated. 

```
GET /link/hello.txt HTTP/1.1
Host: www.website.com
If-modified-since: Wed, 30 Aug 2023 07:07:06 GMT
```
## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
An HTTP response code is a number that is paired with a message and gives information about what happened after the HTTP request was made. They are used to inform the browser and the user of the status of the HTTP response.

- `200 OK`: Request was successful and response includes what was requested
- `300 Moved Permanently`: The requested object was moved permanently. This code also provides the new `Location:` in the header of the response message.
- `400 Bad Request`: A generic error code that is sent when the server cannot comprehend the request.
- `404 Not Found`: The requested object does not exist on this server
- `505 HTTP Version Not Supported`: The requested HTTP protocol cannot be supported by the server.
## I can explain what cookies are used for and how they are implemented.
Cookies are a way to store a user's information. The four elements to cookie technology are: a cookie header line in the HTTP response message, a cookie header in the HTTP request message, a cookie file kept on the client side (user's end device), and a database owned by the webiste. Whenever a new user accesses a cookie-enabled site, it creates a new unique identification number that is placed in the `Set-cookie` field of the HTTP response message. Every time the same user accesses the site, the same cookie number is used in the HTTP response and request messages. When the browser sees the HTTP response message, it adds more information about what the user did on the site to the special cookie file. The cookie file contains all the information that was recorded in the many response and request exchanges between the browser and the web server. Generally, cookies are used by commercial sites to determine what advertisements and recommendations they should display to each user to encourage them to keep shopping. However, cookies are controversial because of the information they track and some may argue that this violates the privacy of Internet users.
## I can explain the significance HTTP pipelining.
HTTP pipelining is used to save time when multiple objects are simultaneously being requested by a client or sent by a server. It would take much more time if a client had to request the next object only after it received the response when getting the previous object. With HTTP persistent connections, the server will keep the TCP connection until a preconfigured amount of time has passed since the last message. This allows for a client and server to exchange multiple requests and responses simultaneously. Without piplelining and persistent connections, a new TCP connection would have to be established each time an object is requested. 
## I can explain the significance of CDNs, and how they work.
CDNs (Content Distribution Networks) were introduced to mitigate the challenge of exchanging large amounts of video streaming data across the world. Without CDNs, there would be freezing delays for users, lots of network bandwidth wasted, and a single point of failure at the distributing data center. CDNs manage servers across a pool of geographic locations and store copies of videos and other web content (documents, images, audio files). They can be private CDNs which are owned by individual content providers (e.g Google) or third-party CDNs which are used by multiple content providers (e.g Limelight, Akamai). The two main CDN server placement philosophies are Enter Deep and Bring Home. Enter Deep seeks to spread clusters of CDNs to access ISPs in order to decrease the proximity between CDN servers and users. This decreases delay on the user side because the data does not have to travel far to the user. However, this design philosophy is hard to maintain. Bring Home seeks to decrease the number of clusters built by placing them in Internet Exchange Points (IXPs). This design philosophy is easier to maintain, but may cause higher delay on the user side. CDNs are very similar to web caching in that they store copies of objects and evict copies that are in least demand to efficiently use their storage.