# Week 9 Learning Goals
## I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.
Link-State algorithms are algorithms that store global state information, meaning the algorithm is aware of each cost in each link of the network. A common practice to retrieve the link costs is using a link-state broadcast where each node broadcasts its link-state packets, containing costs of directly attached links, to all hosts in the network. Every node has the same data about the link costs across the network. An example of a link-state algorithm is Dijkstra's algorithm which computes the path with least cost from one node to all other nodes in the network. A table like this can be used to help compute the least costs to nodes.

![Alt text](dijkstra.png)

One of the advantanges of Link-State algorithms is that they can be easily optimized by using a heap data structure instead of computer linearly. A disadvantage of this algorithm is that oscillations between routers can occur in congestion-sensitive routing. In congestion-sensitive routing, link costs may depend on the direction of where the traffic is going and the size of the traffic. In some cases, routers may find a lesser cost path that goes in the opposite direction the traffic was going, producing oscillations and preventing traffic to reach their destinations. The most reasonable solution to this is making sure that not all routers are running the Link-State algorithm at the same time. This would provide differing execution instances at each node, preventing possible oscillations.
## I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.
Distance-Vector algorithms are iterative, asynchronous, and distributed. Nodes receive information from direct neighbors, perform calculations, and send information received from calculations back to the neighbors. Until no more messages are received from direct neighbors, the process continues. Each node uses this algorithm $d_x(y) = min_v {c(x,v) + d_v (y)}$ to find the shortest path going from node x to node y. $d_v (y)$  is the distance from each direct neighboring node of x to node y. If any distances change, they will be updated and sent to the neighboring nodes.

![Alt text](distancevector.png)

This table shows how distances are calculated and then updated.

One of the main problems of the distance-vector algorithm is the count-to-infinity problem. If a link cost is increased, a routing loop may occur where a link cost of an adjacent node is computed incorrectly. 

![Alt text](countinfinity.png)

In this example, a routing loop appears between nodes y and z, since both nodes are under the impression that the opposite direction is the shortest path to x. This is because the link cost from y to x is computed incorrectly as 6 instead of 51. The incorrect least cost will continue increasing by 1 as it is updated to y's neighbors. Until the link cost reaches 50, the routing loop will occur. To solve this problem, a poisoned reverse can be added. This is done by advertising a node's cost to another node as infinity if it has to go through the same node that it is sending link cost information to. Z will advertise to y that it's cost to x is infinity, avoiding the loop scenario.