# Week 9 Chapter Questions
## R5 ​What is the “count to infinity” problem in distance vector routing?
One of the main problems of the distance-vector algorithm is the count-to-infinity problem. If a link cost is increased, a routing loop may occur where a link cost of an adjacent node is computed incorrectly. This happens when the adjacent node's least path to the destination node has to go through the current node and the current node's least path appears to go through the adjacent node.

![Alt text](countinfinity.png)

In this example, a routing loop appears between nodes y and z, since both nodes are under the impression that the opposite direction is the shortest path to x. This is because the link cost from y to x is computed incorrectly as 6 instead of 51. The incorrect least cost will continue increasing by 1 as it is updated to y's neighbors. Until the link cost reaches 50, the routing loop will occur. To solve this problem, a poisoned reverse can be added. This is done by advertising a node's cost to another node as infinity if it has to go through the same node that it is sending link cost information to. Z will advertise to y that it's cost to x is infinity, avoiding the loop scenario.
## R8 True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
False, an OSPF-configured router broadcasts it's link state information to all routers in the area, not just the neighboring routers. It broadcasts this information when link-state information changes and also periodically every 30 minutes. This is to ensure robustness of the link state algorithm.
## R9 What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
Areas are used as a hierarchy within a single OSPF autonomous system. Routers in the same area have their own link-state algorithm and will broadcast their link state to the other routers in the area. Areas can be connected with border routers, which router packets to other areas. Routing between areas is controlled by the backbone area. Areas were introduced to divide a bigger autonomous system into smaller parts to increase efficiency. Otherwise, routers would have to broadcast link state information to all routers in the autonomous system.
## P3 Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.​
![Alt text](p3.png)

![Alt text](p3sol.png)
## P4 Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following: (only nodes v, y, and w)
### a. Node v
![Alt text](4a.png)
### b. Node y
![Alt text](4b.png)
### c. Node w
![Alt text](4c.png)
## P5 Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.
![Alt text](p5.png)
![Alt text](p5sol.png)
## P11 Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x, y) = 4, c(x,z) = 50, c(y, w) = 1, c(z, w) = 1, c(z, y) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.  
### a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?
![Alt text](p11.png)

$D_w (x)$ sends to y =  infinity because it has to go through y to get to x
$D_w (x)$ sends to z =  5
$D_y (x)$ sends to w and z =  4
$D_z (x)$ sends to w =  7
$D_z (x)$ sends to y =  infinity because it has to go through y to get to x
### b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.
There will not be a count-to-infinity problem. When y sends its c(x, y) to w and z, w and z will update their costs accordingly too. Z's distance to x will update to 50, since it is shorter than y's 60. W's distance to x will update to 53 because it notices that z's distance to x has changed to 50 and y's distance to x changed to 60.
### c. How do you modify c(y, z) such that there is no count-to-infinity problem at all if c(y, x) changes from 4 to 60?
c(y, z) could be changed to 15 so that z's shortest path to x does not have to go through y. As a result, z does not have to advertise its cost to x as infinity to y because it does not go through y.