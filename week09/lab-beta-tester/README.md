# Lab: Beta Tester
## Introduction
This week, I beta tested Protocol Pioneer chapter 5.
## Citation
No other outside resources were used.
## Process
I followed the instructions for setup as specified in the game's [gitlab](https://gitlab.com/jldowns-usna/protocol-pioneer/-/tree/main?ref_type=heads) and continued to follow instructions specified in the actual chapters.
## Question
###  1. How did you solve Chapter 5? Please copy and paste your winning strategy, and also explain it in English.
```
def drone_strategy(self):
    """Drones are responsible for routing messages."""
    counter = 0
    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    number_of_connected_interfaces = len(self.connected_interfaces())
    print(f"Drone {self.id} has {number_of_connected_interfaces} connected interfaces: {self.connected_interfaces()}")

    
    while (self.message_queue):
        m = self.message_queue.pop()
        if "E" in self.connected_interfaces():
            self.send_message(m.text, "E")
        elif "S" in self.connected_interfaces():
            self.send_message(m.text, "S")



def player_strategy(self):
    """This is you. You have access to the key-generating machine. You need to send that key to Sawblade."""
    # `self.generate_key()` generates a new key. The function returns a tuple: (key, expiration). The first value
    # is the actual key (a number). The second value is the time that the key expires.
    # Every time you call the function, a new key is generated and the expiration date is set 18 ticks into the future.
    if "key" not in self.state:
        key_expiration = self.generate_key()
        self.state["key"] = key_expiration[0]
        expiration = key_expiration[1]
        key = self.state["key"]
        print(f"Player: key {key} expires at {expiration}.")

        self.send_message(str(self.state["key"]) + "," + str(expiration), "E")

    # If you want, you can use your heartbeat to count ticks.
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
        time = self.state["ticks"]
    print(f"The current time is {self.state['ticks']}...")

def sawblade_strategy(self):
    """Sawblade operates the radio, but need an unexpired key."""
    
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    
    # Sawblade has the `self.operate_radio(key)` method available to her. Call it with the value of the key that the
    # player has. Don't use an expired key, the escape pod will go on permanent lock down!
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Sawblade: Msg on interface {m.interface} ---\n{m.text}\n------------------")

        key = m.text.split(",")

        if (self.state["ticks"]) < int(key[1]):
            self.operate_radio(int(key[0]))
        

s = Chapter5(drone_strategy, player_strategy, sawblade_strategy, animation_frames=10, wait_for_user=False)
s.run()
```

I found the shortest path from my character to Sawblade and I used conditional if-statements to route the drones. The packet will keep moving right until it can't go right anymore, and then it will move south. I also made sure only one key was sent because if another is created, it will nullify all previous keys. Once Sawblade receives the message, he must check that the key is not expired. If it is not expired, he sends it to the radio.
### 2. Beta Testing Notes
No problems found.