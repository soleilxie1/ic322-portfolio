# Week 9: Network Layer - Control Plane

This week we learned about the control plane, including algorithms such as Link-State and Distance-Vector.

- [Learning Goals](learning-goals.md)
- [Chapter Questions](chapter-questions.md)
- [Lab: Beta Tester](lab-beta-tester)
- [Community Garden](https://gitlab.com/mcwieland/ic322-portfolio/-/issues/16)